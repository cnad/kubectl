FROM ubuntu:16.04

WORKDIR /root

RUN apt-get update && apt-get -y install \
    apt-transport-https \
    gnupg \
    curl \
    wget \
    git \
    ca-certificates \
    && rm -rf /var/lib/apt/lists/*

ENV SHELL /usr/bin/zsh

# Install kubectl
RUN curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list && \
    apt-get update && apt-get -y install --no-install-recommends kubectl

RUN mkdir /root/.kube

#install helm
RUN curl -s https://helm.baltorepo.com/organization/signing.asc | apt-key add - && \
  echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee -a /etc/apt/sources.list.d/helm-stable-debian.list && \
  apt-get update && apt-get -y install --no-install-recommends helm && helm version

ENTRYPOINT ["/bin/zsh"]